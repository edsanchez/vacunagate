import VACUNA_USERS from './user.data';
export const getVacunadosPorProyecto = () => {
  let proyectos = new Map();

  VACUNA_USERS.forEach((vacunado, i) => {
    const numVacunados = proyectos.has(vacunado.proyecto) ?
      proyectos.get(vacunado.proyecto) :
      0;

    proyectos.set(vacunado.proyecto, numVacunados + 1);
  });

  proyectos = Array.from(proyectos);
  return proyectos.map(function(proyecto) {
    return {
      "id": proyecto[0],
      "value": proyecto[1]
    };
  });
}

export const getVacunadosPorObservacion = () => {
  let observaciones = new Map();

  VACUNA_USERS.forEach((vacunado, i) => {
    const numVacunados = observaciones.has(vacunado.observacion) ?
      observaciones.get(vacunado.observacion) :
      0;

    observaciones.set(vacunado.observacion, numVacunados + 1);
  });

  observaciones = Array.from(observaciones);
  return observaciones.map(function(observacion) {
    return {
      "id": observacion[0],
      "value": observacion[1]
    };
  });
}

export const getVacunadosPorDosis = () => {
  let groupDosis = new Map();

  VACUNA_USERS.forEach((vacunado, i) => {
    let numDosis = 0
    if (vacunado.dosis_1) numDosis++;
    if (vacunado.dosis_2) numDosis++;
    if (vacunado.dosis_3) numDosis++;

    const numVacunados = groupDosis.has(numDosis) ?
      groupDosis.get(numDosis) :
      0;

    groupDosis.set(numDosis, numVacunados + 1);
  });

  groupDosis = Array.from(groupDosis);
  return groupDosis.sort((first, second) => {
    return first[0] - second[0];
  }).map(function(group) {
    return {
      "numDosis": group[0] + " dosis",
      "cantidad de vacunados": group[1]
    };
  });
}

export const getVacunadosPorEdad = () => {
  let edades = new Map();

  const grupos = {
    "0": "menor de 25",
    "1": "entre 25 y 35",
    "2": "entre 35 y 45",
    "3": "entre 45 y 55",
    "4": "entre 55 y 65",
    "5": "entre 65 y 75",
    "6": "mayor de 75",
    "7": "no precisa"
  };

  VACUNA_USERS.forEach((vacunado, i) => {
    let edad = '';

    if (!vacunado.edad) edad = 7;
    else if (vacunado.edad < 25) edad = 0;
    else if (vacunado.edad >=25 && vacunado.edad < 35) edad = 1;
    else if (vacunado.edad >=35 && vacunado.edad < 45) edad = 2;
    else if (vacunado.edad >=45 && vacunado.edad < 55) edad = 3;
    else if (vacunado.edad >=55 && vacunado.edad < 65) edad = 4;
    else if (vacunado.edad >=65 && vacunado.edad < 75) edad = 5;
    else if (vacunado.edad >=75) edad = 6;

    const numVacunados = edades.has(edad) ?
      edades.get(edad) :
      0;

    edades.set(edad, numVacunados + 1);
  });

  edades = Array.from(edades);
  edades = edades.sort((first, second) => {
      return first[0] - second[0];
    }).map(function(edad) {
    return {
      "edad": grupos[edad[0]],
      "cantidad de vacunados": edad[1]
    };
  });
  return edades;
}
