import React from 'react';
import { Link } from 'react-router-dom';
import {Switch, Route, Redirect} from 'react-router-dom';
import {connect} from 'react-redux';

import './App.css';

import Header from './components/header/header.component';
import GraphsPage from './pages/graphs-page/graphs-page.component';
import TablePage from './pages/table-page/table-page.component';

class App extends React.Component {
  render() {
    return (
      <div>
        <Header />
        <Switch>
          <Route exact path="/table" component={TablePage} />
          <Route path="/" component={GraphsPage} />
        </Switch>
      </div>
    );
  }

}

export default App;
