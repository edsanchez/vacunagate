import React from 'react';
import { Link } from 'react-router-dom';

import './header.styles.scss';

const Header = () => (
  <div className="header">
    <div className="header-container">
      <div className="header-title">
        <span>#VacunaGate</span>
      </div>
      <div className="header-menu">
        <Link className="header-menu-item" to="/graphs">Gráficos</Link>
        <Link className="header-menu-item" to="/table">Tabla</Link>
      </div>
    </div>
  </div>
);

export default Header;
