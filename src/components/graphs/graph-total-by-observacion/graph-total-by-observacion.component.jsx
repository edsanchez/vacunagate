import React from 'react';
import { ResponsivePie } from '@nivo/pie'
import {getVacunadosPorObservacion} from '../../../redux/user/user.selectors';

import './graph-total-by-observacion.styles.scss';

const GraphTotalByObservacion = () => (
  <div className="graph-total-by-observacion">
    <ResponsivePie
      data={getVacunadosPorObservacion()}
      margin={{ top: 40, right: 80, bottom: 40, left: 80 }}
      innerRadius={0.5}
      padAngle={0.7}
      cornerRadius={3}
      colors={{ scheme: 'nivo' }}
      borderWidth={1}
      borderColor={{ from: 'color', modifiers: [ [ 'darker', 0.2 ] ] }}
      radialLabelsSkipAngle={10}
      radialLabelsTextColor="#333333"
      radialLabelsLinkColor={{ from: 'color' }}
      sliceLabelsSkipAngle={10}
      sliceLabelsTextColor="#333333"
    />
  </div>
);

export default GraphTotalByObservacion;
