import React from 'react';
import { ResponsiveBar } from '@nivo/bar'
import './graph-total-by-dosis.styles.scss';
import {getVacunadosPorDosis} from '../../../redux/user/user.selectors';

const GraphTotalByDosis = () => (
  <div className="graph-total-by-dosis">
    <ResponsiveBar
      data={getVacunadosPorDosis()}
      keys={[ 'cantidad de vacunados']}
      indexBy="numDosis"
      margin={{ top: 50, right: 30, bottom: 50, left: 60 }}
      padding={0.3}
      valueScale={{ type: 'linear' }}
      indexScale={{ type: 'band', round: true }}
      colors={{ scheme: 'nivo' }}
    />
  </div>
);

export default GraphTotalByDosis;
