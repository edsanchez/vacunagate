import React from 'react';
import { ResponsiveBar } from '@nivo/bar'
import {getVacunadosPorEdad} from '../../../redux/user/user.selectors';
import './graph-total-by-ages.styles.scss';

const GraphTotalByAges = () => (
  <div className="graph-total-by-ages">
    <ResponsiveBar
      data={getVacunadosPorEdad()}
      keys={[ 'cantidad de vacunados']}
      indexBy="edad"
      margin={{ top: 50, right: 30, bottom: 50, left: 80 }}
      padding={0.3}
      layout="horizontal"
      valueScale={{ type: 'linear' }}
      indexScale={{ type: 'band', round: true }}
      colors={{ scheme: 'nivo' }}
    />
  </div>
);

export default GraphTotalByAges;
