import React from 'react';
import { useTable, useSortBy, usePagination } from 'react-table';
import VACUNA_USERS from '../../redux/user/user.data';
import './table-page.styles.scss';

const TablePage = () => {
  const data = React.useMemo(
     () => VACUNA_USERS,
     []
   )

   const columns = React.useMemo(
     () => [
       {
         Header: 'N°',
         accessor: 'orden',
       },
       {
         Header: 'Apellidos',
         accessor: 'apellidos',
       },
       {
         Header: 'Nombres',
         accessor: 'nombres',
       },
       {
         Header: 'Edad',
         accessor: 'edad',
       },
       {
         Header: 'Primera dosis',
         accessor: 'dosis_1',
       },
       {
         Header: 'Segunda dosis',
         accessor: 'dosis_2',
       },
       {
         Header: 'Tercera dosis',
         accessor: 'dosis_3',
       },
       {
         Header: 'Observación',
         accessor: 'observacion',
       },
       {
         Header: 'Proyecto',
         accessor: 'proyecto',
       },
     ],
     []
   );

   const {
     getTableProps,
    getTableBodyProps,
    headerGroups,
    prepareRow,
    page,
    canPreviousPage,
    canNextPage,
    pageOptions,
    pageCount,
    gotoPage,
    nextPage,
    previousPage,
    setPageSize,
    state: { pageIndex, pageSize },
  } = useTable({ columns, data, initialState: { pageSize: 40, pageIndex: 0 } }, useSortBy, usePagination);

  return (
    <div className="table-page">
      <div className="container">
        <table className="table-vacunas" {...getTableProps()}>
          <thead>
            {headerGroups.map(headerGroup => (
              <tr {...headerGroup.getHeaderGroupProps()}>
                {headerGroup.headers.map(column => (
                  <th className="table-vacunas-column-header"
                    {...column.getHeaderProps(column.getSortByToggleProps())}
                    >
                    {column.render('Header')}
                    <span>
                      {column.isSorted
                        ? column.isSortedDesc
                          ? ' 🔽'
                          : ' 🔼'
                        : ''}
                    </span>
                  </th>
                ))}
              </tr>
            ))}
          </thead>
          <tbody {...getTableBodyProps()}>
            {page.map((row, i) => {
              prepareRow(row)
              return (
                <tr {...row.getRowProps()}>
                  {row.cells.map(cell => {
                    return (
                      <td
                        {...cell.getCellProps()}
                        >
                        {cell.render('Cell')}
                      </td>
                    )
                  })}
                </tr>
              )
            })}
          </tbody>
        </table>
        <div className="pagination">
          <button onClick={() => gotoPage(0)} disabled={!canPreviousPage}>
            {'<<'}
          </button>{' '}
          <button onClick={() => previousPage()} disabled={!canPreviousPage}>
            {'<'}
          </button>{' '}
          <button onClick={() => nextPage()} disabled={!canNextPage}>
            {'>'}
          </button>{' '}
          <button onClick={() => gotoPage(pageCount - 1)} disabled={!canNextPage}>
            {'>>'}
          </button>{' '}
          <span>
            Page{' '}
            <strong>
              {pageIndex + 1} of {pageOptions.length}
            </strong>{' '}
          </span>
          <span>
            | Go to page:{' '}
            <input
              type="number"
              defaultValue={pageIndex + 1}
              onChange={e => {
                const page = e.target.value ? Number(e.target.value) - 1 : 0
                gotoPage(page)
              }}
              style={{ width: '100px' }}
            />
          </span>{' '}
          <select
            value={pageSize}
            onChange={e => {
              setPageSize(Number(e.target.value))
            }}
          >
            {[20, 30, 40, 50, 100, 500].map(pageSize => (
              <option key={pageSize} value={pageSize}>
                Show {pageSize}
              </option>
            ))}
          </select>
        </div>
      </div>
    </div>
  )
};

export default TablePage;
