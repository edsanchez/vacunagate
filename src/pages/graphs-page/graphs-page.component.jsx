import React from 'react';
import GraphTotalByProyect from '../../components/graphs/graph-total-by-project/graph-total-by-project.component';
import GraphTotalByObservacion from '../../components/graphs/graph-total-by-observacion/graph-total-by-observacion.component';
import GraphTotalByDosis from '../../components/graphs/graph-total-by-dosis/graph-total-by-dosis.component';
import GraphTotalByAges from '../../components/graphs/graph-total-by-ages/graph-total-by-ages.component';

import './graphs-page.styles.scss';

const GraphsPage = () => (
  <div className="graphs-page">
    <div className="container">
      <div className="graph-item">
        <h2 className="graph-title">Total de vacunados por "proyecto"</h2>
        <div className="graph-body">
          <GraphTotalByProyect/>
        </div>
      </div>
      <div className="graph-item">
        <h2 className="graph-title">Total de vacunados por "observación"</h2>
        <div className="graph-body">
          <GraphTotalByObservacion/>
      </div>
      </div>
      <div className="graph-item">
        <h2 className="graph-title">Total de vacunados por número de dosis</h2>
        <div className="graph-body">
          <GraphTotalByDosis/>
      </div>
      </div>
      <div className="graph-item">
        <h2 className="graph-title">Total de vacunados por edades</h2>
        <div className="graph-body">
          <GraphTotalByAges/>
      </div>
      </div>



    </div>
  </div>
);

export default GraphsPage;
